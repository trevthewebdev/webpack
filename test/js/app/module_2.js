export const func1 = function () {
  return 'your mom goes to func1';
};

export const func2 = function () {
  return 'this function (func2) should be tree shook';
};

export const func3 = function () {
  return 'this function should be tree shook';
};

export default {
  func1,
  func2,
  func3
};
