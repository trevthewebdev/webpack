import $ from 'jquery';

const SomePlugin = function ($elem, { name }) {
  this.$elem = $elem;
  this.name = name;
};

SomePlugin.prototype = {
  defaults: {
    background: 'grey',
  },

  init(options) {
    this.options = $.extend({}, this.defaults, options);
    this.changeBackground(this.options);
    this.bindThings();
  },

  bindThings() {
    this.$elem.on('click', (event) => {
      this.handleClick(event);
    });
  },

  changeBackground({ background }) {
    this.$elem.css({ backgroundColor: background });
  },

  handleClick() {
    this.$elem.html(this.name);
  }
};

export default $.fn.SomePlugin = function (options) {
  return new SomePlugin(this, options).init(options);
};
