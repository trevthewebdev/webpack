import $ from 'jquery';
import { func1 } from './app/module_2';

// Imports Custom jQuery Plugin
require('./app/module_1');

// Example of tree-shaking by importing & using one function
func1();

$('#container').SomePlugin({
  name: 'something',
  background: 'blue',
});

$('#container-2').SomePlugin({
  name: 'something else',
});
