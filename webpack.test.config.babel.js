const _ = require('lodash');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const sharedWebpackConfig = require('./webpack.vars.babel');

module.exports = function (env) {
  return _.mergeWith({}, sharedWebpackConfig.commonConfig(env), {

    // Specify different entries (files) that will be use
    entry: {

      // Javascript
      main: './index.js',
      vendor_head: sharedWebpackConfig.testVendorHeadFiles,
      vendor: sharedWebpackConfig.testVendorBodyFiles,

      // Css Themes
      dark: './test/scss/themes/dark.scss',
      light: './test/scss/themes/light.scss',
    },

    plugins: [

      // Only load the manifest for the HEAD
      new HtmlWebpackPlugin({
        inject: false,
        chunks: ['vendor_head', 'manifest', 'dark'],
        template: path.resolve(__dirname, 'test/pug/header.pug'),
        filename: 'header.html'
      }),

      // Load everything else for the FOOTER
      new HtmlWebpackPlugin({
        inject: false,
        excludeChunks: ['manifest', 'vendor_head', 'dark', 'light'],
        template: path.resolve(__dirname, 'test/pug/footer.pug'),
        filename: 'footer.html'
      }),

      // TODO: This needs to be moved to development / test mode only
      new HtmlWebpackPlugin({
        inject: false,
        template: path.resolve(__dirname, 'test/pug/index.pug'),
        filename: 'index.html'
      })
    ]

  }, sharedWebpackConfig.mergeArrays);
};
