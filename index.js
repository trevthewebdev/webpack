import $ from 'jquery';
import { func1 } from './test/js/app/module_2';

// Imports Custom jQuery Plugin
require('./test/js/app/module_1');

// Example of tree-shaking by importing & using one function
func1();

$('#container').SomePlugin({
  name: 'something',
  background: 'blue',
});

$('#container-2').SomePlugin({
  name: 'something else',
});
