const _ = require('lodash');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const sharedWebpackConfig = require('./webpack.vars.babel');

module.exports = function (env) {
  return _.mergeWith({}, sharedWebpackConfig.commonConfig(env), {

    // Specify different entries (files) that will be use
    entry: {

      // Javascript
      main: './index.js',
      vendor_head: sharedWebpackConfig.appVendorHeadFiles,
      vendor: sharedWebpackConfig.appVendorBodyFiles,
    },

    plugins: [

      // Only load the manifest for the HEAD
      new HtmlWebpackPlugin({
        inject: false,
        chunks: ['vendor_head', 'manifest'],
        template: path.resolve(__dirname, 'src/pug/header.pug'),
        filename: 'header.html'
      }),

      // Load everything else for the FOOTER
      new HtmlWebpackPlugin({
        inject: false,
        excludeChunks: ['manifest', 'vendor_head'],
        template: path.resolve(__dirname, 'src/pug/footer.pug'),
        filename: 'footer.html'
      })
    ]
  }, sharedWebpackConfig.mergeArrays);
};
