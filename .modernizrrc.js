// More Details at : https://www.npmjs.com/package/webpack-modernizr-loader
// Add features for modernizr here since it's NPM package instead of download
module.exports = {
    options: [],
    "feature-detects": [
        "test/css/flexbox"
    ]
};