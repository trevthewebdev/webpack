module.exports = {
  "extends": "airbnb-base",
  "plugins": ["import"],
  rules: {
    'max-len': 0,
    'comma-dangle': 0,
    'func-names': 0,
    'import/newline-after-import': 0,
    "import/no-extraneous-dependencies": [
      "error",
      { "devDependencies": true }
    ]
  }
};