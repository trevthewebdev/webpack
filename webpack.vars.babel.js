const _ = require('lodash');
const path = require('path');
const glob = require('glob');
const webpack = require('webpack');
const ManifestPlugin = require('webpack-manifest-plugin');
const CssEntryPlugin = require('css-entry-webpack-plugin');

/* eslint-disable consistent-return */
module.exports.mergeArrays = function mergeArrays(objValue, srcValue) {
  if (_.isArray(objValue)) {
    return objValue.concat(srcValue);
  }
};
/* eslint-enable consistent-return */

/*
 * Common Config
 * Configuration shared between test and dev / prod
 * @Param env: current environment
 */
module.exports.commonConfig = function commonConfig(env) {
  const isProd = env.production === true;

  // Common properties
  return {
    watch: !isProd,
    watchOptions: {
      ignored: /node_modules/
    },

    output: {
      filename: (isProd) ? '[name].[chunkhash].js' : '[name].js',
      path: path.resolve(__dirname, 'dist')
    },

    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
        name: ['vendor', 'manifest']
      }),

      new ManifestPlugin(),

      new CssEntryPlugin({
        extensions: ['.css', '.scss'],
        output: {
          filename: (isProd) ? '[name].[contenthash].css' : '[name].css',
        }
      }),
    ],

    module: {
      loaders: [

        // Configures eslint to run on files before getting transpiled via babel
        {
          test: /\.js$/,
          enforce: 'pre',
          loader: 'eslint-loader',
          exclude: [
            '/node_modules/',
            path.resolve(__dirname, 'src/js/vendor')
          ],
          options: {
            emitWarning: true
          }
        },

        // Transpiles es6 / es2015 into es5
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        },
        {
          test: /\.less$/,
          loader: ['css-loader', 'less-loader']
        },

        {
          test: /\.less$/,
          loader: ['css-loader', 'less-loader']
        },

        {
          test: /\.scss$/,
          loader: ['css-loader', 'sass-loader']
        },

        {
          test: /\.modernizrrc.js$/,
          loader: 'webpack-modernizr-loader?useConfigFile'
        },

        // Used for generating html files with correct hashes and filenames
        {
          test: /\.pug$/,
          loader: ['pug-loader']
        }
      ]
    },

    resolve: {
      alias: {
        modernizr$: path.resolve(__dirname, '.modernizrrc.js')
      }
    }
  };
};

// Test Files
module.exports.testVendorHeadFiles = ['modernizr'];
module.exports.testVendorBodyFiles = [
  'jquery',
  ...glob.sync(path.resolve(__dirname, 'test/js/vendor/*.js'))
];

// App Files
module.exports.appVendorHeadFiles = [
  'modernizr'
];

module.exports.appVendorBodyFiles = [
  'jquery',
  ...glob.sync(path.resolve(__dirname, 'src/js/vendor/*.js'))
];
