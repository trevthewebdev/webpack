Build system using webpack that still allows you to have exteneral files instead
of bundling everything together under bundle.js.

This build system supports caching of vender files (including webpacks runtime).

## Project Setup
### Install project dependencies
`npm install`

## Available Build Commands
### Production build
`npm run build:prod`

### Development Mode / Watch
`npm run build:dev`

### Development of build system & Feature Testing
`npm run test:watch`

### Production mode for build system & Feature Testing
`npm run test:prod`

### Lint the project
`npm run lint`


## Helpful Links
- Es6 / Es2015 features: https://babeljs.io/learn-es2015/
- Modernizr Configuration : https://www.npmjs.com/package/webpack-modernizr-loader